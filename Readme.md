## IDERelease20201110

- Support only ubuntu20.0
- Project creation for vega board and incorporates the sdk examples for baremetal programming
- Support added for  auto external tool script creation for downloading of program on Vega64 board via eth interface
- Addition of right click button for binary upload (buttons opens up the external tool configuration)
- Integrated yocto plugin for Linux Application development

## Setup Demonstration (Video Links) 

- [Vega64 Board Setup](https://youtu.be/vvQBSO4SGZU)
- [Eclipse IDE Setup](https://youtu.be/WdSIxPstJDw) for Linux Programming
- [Eclipse IDE Setup](https://youtu.be/gmuFboSs89A) for Bare-metal Programming